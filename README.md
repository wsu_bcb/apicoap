# Welcome to the new distribution site of ApicoAP software!
Please note that [our older site](https://code.google.com/p/apicoap/) will no longer be maintained.

In order to access the previous and the latest versions of ApicoAP software, please follow the [Downloads link](https://bitbucket.org/wsu_bcb/apicoap/downloads).

In order to access the user manuals, please follow the [Wiki link](https://bitbucket.org/wsu_bcb/apicoap/wiki/Home). Here are some direct links to user manuals:

[ApicoAP (version 1) User Manual](https://bitbucket.org/wsu_bcb/apicoap/wiki/ApicoAP%20(version%201)%20User%20Manual)

[ApicoAP (version 2-3) User Manual](https://bitbucket.org/wsu_bcb/apicoap/wiki/ApicoAP%20(version%202)%20User%20Manual)

[Instructions for converting ApicoAP_GUI.py to MAC OS X compatible .app file](https://bitbucket.org/wsu_bcb/apicoap/wiki/Instructions%20for%20converting%20ApicoAP_GUI.py%20to%20MAC%20OS%20X%20compatible%20.app%20file)

###Please report any bugs you encountered using the software to gokcen.cilingir@gmail.com###

###Please check out [the info](https://bitbucket.org/wsu_bcb/apicoap/wiki/Problems%20regarding%20SignalP%20server) explaining what to do when ApicoAP tool gives an error saying "SignalP server is not responding".

###You can find more information over the method and the software at the following reference:

[Cilingir, Gokcen, Shira L. Broschat, and Audrey OT Lau. "ApicoAP: the first computational model for identifying apicoplast-targeted proteins in multiple species of Apicomplexa." PloS one 7.5 (2012): e36598-e36598](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0036598)

If you're using version 3 (latest), please also cite:

[Cilingir, Gokcen, and Shira L. Broschat. "Automated Training for Algorithms That Learn from Genomic Data." BioMed research international 2015 (2015).](http://www.hindawi.com/journals/bmri/2015/234236/abs/)
